import os
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    try:
        if os.uname().sysname == "SunOS":
            return hub
    except:
        ...

    pytest.skip("idem-solaris is only intended for SunOs systems")
