from dict_tools import data
import pytest
import mock

NETSTAT_IP4_DATA = """
Routing Table: IPv4
  Destination           Gateway           Flags  Ref     Use     Interface
-------------------- -------------------- ----- ----- ---------- ---------
default              192.168.1.1          UG        2        389 net0
aurelion             aurelion             UH        2        314 lo0
192.168.1.0          192.168.1.16         U         4      51671 net0
192.168.1.13         192.168.1.13         U         4      51671 net1
"""

NETSTAT_IP6_DATA = """
Routing Table: IPv6
  Destination/Mask            Gateway                   Flags Ref   Use    If
--------------------------- --------------------------- ----- --- ------- -----
aurelion                    aurelion                    UH      2     114 lo0
fe80::/10                   fe80::a00:ffff:ffff:ffff    U       2       0 net0
"""


@pytest.mark.asyncio
async def test_load_default_gateway(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": NETSTAT_IP4_DATA}),
        data.NamespaceDict({"stdout": NETSTAT_IP6_DATA}),
    ]

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.net.gateway.load_default_gateway = (
            hub.grains.solaris.net.gateway.load_default_gateway
        )
        await mock_hub.grains.solaris.net.gateway.load_default_gateway()

    assert mock_hub.grains.GRAINS.ip_gw is True
    assert mock_hub.grains.GRAINS.ip4_gw == "192.168.1.1"
    assert mock_hub.grains.GRAINS.ip6_gw is False
