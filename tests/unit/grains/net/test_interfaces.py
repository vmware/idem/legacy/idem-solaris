from dict_tools import data
import pytest
import mock

IFCONFIG_DATA = """
lo0: flags=2001000849<UP,LOOPBACK,RUNNING,MULTICAST,IPv4,VIRTUAL> mtu 8232 index 1
        inet 127.0.0.1 netmask ff000000
net0: flags=100001004843<UP,BROADCAST,RUNNING,MULTICAST,DHCP,IPv4,PHYSRUNNING> mtu 1500 index 2
        inet 192.168.1.16 netmask ffffff00 broadcast 192.168.1.255
lo0: flags=2002000849<UP,LOOPBACK,RUNNING,MULTICAST,IPv6,VIRTUAL> mtu 8252 index 1
        inet6 ::1/128
net1: flags=120002004841<UP,RUNNING,MULTICAST,DHCP,IPv6,PHYSRUNNING> mtu 1500 index 2
        inet6 fe80::a00:ffff:ffff:ffff/10
"""

NETSTAT_INET_DATA = """
Net to Media Table: IPv4
Device   IP Address               Mask      Flags      Phys Addr
------ -------------------- --------------- -------- ---------------
net0   192.168.1.14         255.255.255.255          f0:7b:cb:41:4d:1f
net0   192.168.1.1          255.255.255.255          9c:3d:cf:2b:5a:d0
net0   192.168.1.16         255.255.255.255 SPLA     08:00:27:41:3b:08
"""

NETSTAT_INET6_DATA = """
Net to Media Table: IPv6
 If   Physical Address    Type      State      Destination/Mask
----- -----------------  ------- ------------ ---------------------------
net0  33:33:00:00:00:01  other   REACHABLE    ff02::1
net0  33:33:00:00:00:02  other   REACHABLE    ff02::2
net0  33:33:00:01:00:02  other   REACHABLE    ff02::1:2
net0  33:33:00:00:00:16  other   REACHABLE    ff02::16
net1  08:00:27:41:3b:08  local   REACHABLE    fe80::a00:ffff:ffff:ffff
"""


@pytest.mark.asyncio
async def test_load_interfaces_ifconfig(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": NETSTAT_INET_DATA}),
        data.NamespaceDict({"stdout": NETSTAT_INET6_DATA}),
        data.NamespaceDict({"stdout": IFCONFIG_DATA}),
    ]

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.solaris.net.interfaces.load_interfaces = (
            hub.grains.solaris.net.interfaces.load_interfaces
        )
        await mock_hub.grains.solaris.net.interfaces.load_interfaces()

    assert mock_hub.grains.GRAINS.hwaddr_interfaces._dict() == {
        "net0": "08:00:27:41:3b:08",
        "net1": "08:00:27:41:3b:08",
    }
    assert mock_hub.grains.GRAINS.ip4_interfaces._dict() == {
        "lo0": ("127.0.0.1",),
        "net0": ("192.168.1.16",),
    }
    assert mock_hub.grains.GRAINS.ip6_interfaces._dict() == {
        "lo0": ("::1",),
        "net1": ("fe80::a00:ffff:ffff:ffff",),
    }
    assert mock_hub.grains.GRAINS.ip_interfaces._dict() == {
        "lo0": ("127.0.0.1", "::1"),
        "net0": ("192.168.1.16",),
        "net1": ("fe80::a00:ffff:ffff:ffff",),
    }
    assert mock_hub.grains.GRAINS.ipv4 == ("127.0.0.1", "192.168.1.16")
    assert mock_hub.grains.GRAINS.ipv6 == ("::1", "fe80::a00:ffff:ffff:ffff")
