import pytest
import mock


@pytest.mark.asyncio
async def test_load_uname(mock_hub, hub):
    with mock.patch(
        "os.uname",
        return_value=(
            "SunOS",
            "testname",
            "testrelease",
            "joyent_testversion",
            "testarch",
        ),
    ):
        with mock.patch("sys.platform", "sunos"):
            mock_hub.grains.solaris.uname.load_uname = (
                hub.grains.solaris.uname.load_uname
            )
            await mock_hub.grains.solaris.uname.load_uname()

    assert mock_hub.grains.GRAINS.kernel == "SunOS"
    assert mock_hub.grains.GRAINS.nodename == "testname"
    assert mock_hub.grains.GRAINS.kernelrelease == "testrelease"
    assert mock_hub.grains.GRAINS.kernelversion == "joyent_testversion"
    assert mock_hub.grains.GRAINS.osarch == "testarch"

    assert mock_hub.grains.GRAINS.smartos is True
