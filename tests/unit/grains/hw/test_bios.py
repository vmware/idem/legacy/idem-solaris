from dict_tools import data
import pytest

PRTCONF_DATA = """
System Configuration: innotek GmbH VirtualBox
BIOS Configuration: innotek GmbH VirtualBox 12/01/2006

System PROM revisions:
----------------------
OBP 3.15.2 1998/11/10 10:35 POST 2.3.1 1998/08/07 16:33

chassis-sn:  'test_serial_number'
name:  SUNW,Sun-Fire-T200
banner-name:  Sun Fire T200
"""

PRTDIAG_DATA = """
System Configuration: Sun Microsystems  sun4u Netra 440
System clock frequency: 177 MHZ
Memory size: 8GB

Chassis Serial Number
---------------------
test_serial_number

================================ HW Revisions ================================
ASIC Revisions:
---------------
pci: Rev 4
pci: Rev 4
pci: Rev 4
pci: Rev 4

System PROM revisions:
----------------------
OBP 4.22.33 2007/06/18 12:42 Sun Fire V440,Netra 440
OBDIAG 4.22.33 2007/06/18 12:57
"""

VIRTINFO_DATA = """
Domain role: LDoms control I/O service root
Domain name: primary
Domain UUID: 8e0d6ec5-cd55-e57f-ae9f-b4cc050999a4
Control domain: san-t2k-6
Chassis serial#: 0704RB0280
"""


@pytest.mark.asyncio
async def test_load_bios_data_prtconf(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PRTCONF_DATA})

    mock_hub.grains.solaris.hw.bios.load_bios_data = (
        hub.grains.solaris.hw.bios.load_bios_data
    )

    await mock_hub.grains.solaris.hw.bios.load_bios_data()

    assert mock_hub.grains.GRAINS.biosreleasedate == "1998/11/10"
    # assert mock_hub.grains.GRAINS.biosversion == ""
    # assert mock_hub.grains.GRAINS.manufacturer == ""
    assert mock_hub.grains.GRAINS.productname == "Sun Fire T200"
    assert mock_hub.grains.GRAINS.serialnumber == "test_serial_number"
    # assert mock_hub.grains.GRAINS.uuid == ""


@pytest.mark.asyncio
async def test_load_bios_data_prtdiag(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PRTDIAG_DATA})

    mock_hub.grains.solaris.hw.bios.load_bios_data = (
        hub.grains.solaris.hw.bios.load_bios_data
    )

    await mock_hub.grains.solaris.hw.bios.load_bios_data()

    # assert mock_hub.grains.GRAINS.biosreleasedate == ""
    assert mock_hub.grains.GRAINS.biosversion == "4.22.33"
    assert mock_hub.grains.GRAINS.manufacturer == "Sun Microsystems"
    assert mock_hub.grains.GRAINS.productname == "Netra 440"
    assert mock_hub.grains.GRAINS.serialnumber == "test_serial_number"
    # assert mock_hub.grains.GRAINS.uuid == ""


@pytest.mark.asyncio
async def test_load_bios_data_virtinfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": VIRTINFO_DATA})

    mock_hub.grains.solaris.hw.bios.load_bios_data = (
        hub.grains.solaris.hw.bios.load_bios_data
    )

    await mock_hub.grains.solaris.hw.bios.load_bios_data()

    # assert mock_hub.grains.GRAINS.biosreleasedate == ""
    # assert mock_hub.grains.GRAINS.biosversion == ""
    # assert mock_hub.grains.GRAINS.manufacturer == ""
    # assert mock_hub.grains.GRAINS.productname == ""
    assert mock_hub.grains.GRAINS.serialnumber == "0704RB0280"
    assert mock_hub.grains.GRAINS.uuid == "8e0d6ec5-cd55-e57f-ae9f-b4cc050999a4"
